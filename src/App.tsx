import Product from "./pages/Product";
import productData from "./json/data.json";
import { createTheme, ThemeProvider } from "@mui/material";

let theme = createTheme({
  typography: {
    fontFamily: [
      "Noto Sans JP",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
  palette: {
    custom: {
      icon: "#a1a1a1",
      greyText: "#8d8d8d",
      greyBg: "#efefef",
      link: "#3d7fa5",
    },
  },
});

theme = createTheme(theme, {
  palette: {
    orange: theme.palette.augmentColor({
      color: {
        main: "#e14f42",
      },
      name: "orange",
    }),
  },
});

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Product data={productData} />
    </ThemeProvider>
  );
};

export default App;
