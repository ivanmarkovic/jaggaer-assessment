import React from "react";
import { Box, Button, TextField, Typography } from "@mui/material";
import AddIcon from "../assets/add.svg?react";

const AddToCart = ({
  onAddToCart,
}: {
  onAddToCart: (arg0: number) => void;
}) => {
  const [pce, setPce] = React.useState(1);
  const onInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setPce(parseInt(e.target.value) || 0);
  };
  return (
    <Box sx={{ display: "flex" }}>
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <TextField
          onChange={onInputChange}
          value={pce}
          size="small"
          variant="outlined"
          sx={{ width: "60px" }}
        />{" "}
        <Typography sx={{ ml: "6px", color: "#000" }}>PCE</Typography>
      </Box>
      <Button
        color="orange"
        variant="contained"
        startIcon={<AddIcon fill="#fff" width={25} />}
        sx={{ ml: "15px", textTransform: "none" }}
        disableElevation
        onClick={() => onAddToCart(pce)}
      >
        Add to Cart
      </Button>
    </Box>
  );
};

export default AddToCart;
