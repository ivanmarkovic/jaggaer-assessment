import { Dialog } from "@mui/material";
import ZoomOutIcon from "../assets/zoom-out.svg?react";

const SimpleDialog = ({
  open,
  onClose,
  children,
}: {
  open: boolean;
  onClose: () => void;
  children: JSX.Element;
}) => {
  return (
    <Dialog onClose={onClose} open={open}>
      <div
        onClick={onClose}
        style={{ position: "absolute", right: 0, bottom: 0, cursor: "pointer" }}
      >
        <ZoomOutIcon width={30} fill="#000" />
      </div>
      {children}
    </Dialog>
  );
};

export default SimpleDialog;
