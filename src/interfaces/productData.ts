export interface IProductData {
  article: IArticle;
  cart: ICart;
  user: {
    favorite_articles: number[];
  };
}

export interface ICart {
  items: number;
  total_costs: number;
}

interface IAttachment {
  file_label: string;
  file_size: number;
  file_name: string;
  file_link: string;
}

export interface IArticle {
  id: number;
  title: string;
  description_short: string;
  description_long: string;
  supplier_name: string;
  supplier_link: string;
  stars: number;
  price: number;
  price_breaks: {
    [key: string]: number;
  };
  currency: string;
  transport_costs: number;
  vat_percent: number;
  images: string[];
  minimum_order_quantity: number;
  delivery_time: number;
  unit: string;
  features: {
    [key: string]: string;
  };
  attachments: IAttachment[];
  keywords: string[];
}
