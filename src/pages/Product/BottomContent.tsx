import { IArticle } from "../../interfaces/productData";
import {
  Box,
  Container,
  Divider,
  Grid,
  Paper,
  Typography,
  List,
  ListItem,
  Link,
  ListItemIcon,
  Chip,
  TableBody,
  TableRow,
  TableCell,
  Table,
} from "@mui/material";
import AttacmentIcon from "../../assets/attachment.svg?react";
import "../../styles/content.css";

const titleText = (title: string) => {
  return (
    <Typography
      variant="body1"
      sx={(theme) => ({
        color: theme.palette.orange.main,
        fontWeight: "bold",
        pb: "6px",
      })}
    >
      {title}
    </Typography>
  );
};
const subtitleText = (subtitle: string) => {
  return (
    <Typography
      variant="subtitle2"
      color={(theme) => theme.palette.custom.greyText}
    >
      {subtitle}
    </Typography>
  );
};
const renderListItem = (text1: string, text2: string) => {
  return (
    <ListItem
      key={`${text1}${text2}`}
      sx={{ display: "list-item", p: "0", ml: 2 }}
    >
      <Box sx={{ display: "flex" }}>
        <Typography
          variant="subtitle2"
          color={(theme) => theme.palette.custom.greyText}
        >
          {text1}: <span style={{ color: "#000" }}>{text2}</span>
        </Typography>
      </Box>
    </ListItem>
  );
};

const BottomContent = ({ data }: { data: IArticle }) => {
  return (
    <Box sx={(theme) => ({ backgroundColor: theme.palette.custom.greyBg })}>
      <Container maxWidth="lg" sx={{ p: "20px", pt: "40px" }}>
        <Box sx={{ maxWidth: "900px", pb: 3 }}>
          {titleText("DESCRIPTION")}
          <Typography>{data.description_long}</Typography>
        </Box>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <Paper elevation={0} square sx={{ p: 2, height: "275px" }}>
              {titleText("DETAILS")}
              <Divider sx={{ mb: 1.5 }} />
              {subtitleText("Features")}
              <List sx={{ listStyleType: "disc", p: 0, pb: 1 }}>
                {Object.keys(data.features).map((feat: string) =>
                  renderListItem(feat, data.features[feat])
                )}
              </List>

              {subtitleText("Attachments")}
              <List sx={{ p: 0, pb: 1 }}>
                {data.attachments.map((att, i) => {
                  return (
                    <ListItem
                      key={i}
                      sx={{ p: "0", display: "flex", alignItems: "center" }}
                    >
                      <ListItemIcon sx={{ minWidth: "20px" }}>
                        <AttacmentIcon width={15} />
                      </ListItemIcon>
                      <Link
                        href={att.file_link}
                        underline="hover"
                        target="_blank"
                        variant="subtitle2"
                        color={(theme) => theme.palette.custom.link}
                      >
                        {att.file_label}
                      </Link>
                    </ListItem>
                  );
                })}
              </List>

              {subtitleText("Keywords")}
              <Box sx={{ pt: 1 }}>
                {data.keywords.map((k, i) => (
                  <Chip label={k} sx={{ mr: 1 }} />
                ))}
              </Box>
            </Paper>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper elevation={0} square sx={{ p: 2, height: "275px" }}>
              {titleText("PRICING & SHIPPING")}
              <Divider sx={{ mb: 1.5 }} />
              <Box sx={{ display: "flex", flexDirection: "column" }}>
                {renderListItem(
                  "Minimum order",
                  `${data.minimum_order_quantity} PCE`
                )}
                {renderListItem(
                  "Shipping",
                  `${data.transport_costs} ${data.currency}`
                )}
                {renderListItem("Delivery", `${data.delivery_time} days`)}
              </Box>
              <Box sx={{ position: "relative", top: "40px" }}>
                {subtitleText("Price breaks")}
                <Table padding="none" sx={{ width: "250px" }}>
                  <TableBody>
                    {Object.keys(data.price_breaks).map(
                      (pb: string, i: number) => {
                        return (
                          <TableRow key={i}>
                            <TableCell sx={{ p: "5px" }}>ex {pb} PCE</TableCell>
                            <TableCell sx={{ p: "5px" }}>
                              {data.price_breaks[pb]} {data.currency}/PCE
                            </TableCell>
                          </TableRow>
                        );
                      }
                    )}
                  </TableBody>
                </Table>
              </Box>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default BottomContent;
