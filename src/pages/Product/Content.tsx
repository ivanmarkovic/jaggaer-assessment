import React from "react";
import {
  Box,
  Container,
  Grid,
  IconButton,
  Link,
  Popover,
  Rating,
  Typography,
  useTheme,
} from "@mui/material";
import { IArticle } from "../../interfaces/productData";
import ZoomInIcon from "../../assets/zoom-in.svg?react";
import StarFilledIcon from "../../assets/star-filled.svg?react";
import StarIcon from "../../assets/star.svg?react";
import PackageIcon from "../../assets/package.svg?react";
import DiscountIcon from "../../assets/discount.svg?react";

import SimpleDialog from "../../components/SimpleDialog";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import "../../styles/content.css";
import AddToCart from "../../components/AddToCart";

interface IContentProps {
  data: IArticle;
  handleAddToCart: (arg0: number) => void;
  moveAddToCartButton: React.Dispatch<React.SetStateAction<boolean>>;
}

const getFill = (index: number) => {
  switch (index) {
    case 0:
      return "#ff0000";
    case 1:
      return "#00ff00";
    case 2:
      return "#0000ff";
    default:
      return "#000";
  }
};

const Content = ({
  data,
  handleAddToCart,
  moveAddToCartButton,
}: IContentProps) => {
  const myTheme = useTheme();
  const [previewImage, setPreviewImage] = React.useState(getFill(0));
  const [showFullImage, setShowFullImage] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const cartRef = React.useRef<HTMLInputElement>(null);

  React.useEffect(() => {
    checkIfCartInView();
  }, []);

  React.useEffect(() => {
    document.addEventListener("scroll", checkIfCartInView);
    return () => {
      document.removeEventListener("scroll", checkIfCartInView);
    };
  }, []);

  const handleSelectPreview = (image: string) => {
    if (image !== previewImage) {
      setPreviewImage(image);
    }
  };
  const checkIfCartInView = () => {
    const box = cartRef?.current?.getBoundingClientRect();
    box &&
      moveAddToCartButton(box.top < window.innerHeight && box.bottom <= 50);
  };
  return (
    <Container maxWidth="lg" sx={{ mt: "70px", p: "20px" }}>
      <Grid container spacing={2} sx={{ pb: "30px" }}>
        <Grid item xs={12} md={5}>
          <Grid container spacing={1}>
            <Grid item xs={3} className="image_list">
              {data.images.map((item, index) => {
                const color = getFill(index);
                return (
                  <Box
                    key={index}
                    onClick={() => handleSelectPreview(color)}
                    sx={(theme) => ({
                      cursor: "pointer",
                      border: `2px solid ${theme.palette.custom.greyBg}`,
                      mb: "5px",
                    })}
                  >
                    <PackageIcon width="100%" height={"100%"} fill={color} />
                  </Box>
                );
              })}
            </Grid>
            <Grid item xs={9}>
              <Box
                className="preview_image"
                onClick={() => setShowFullImage(true)}
                sx={(theme) => ({
                  border: `2px solid ${theme.palette.custom.greyBg}`,
                })}
              >
                <div className="over_preview_image" />
                <PackageIcon width="100%" height={"100%"} fill={previewImage} />
                <div className="zoom_in_icon">
                  <ZoomInIcon width={30} fill={"#fff"} />
                </div>
              </Box>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12} md={4} className="right_content">
          <Box>
            <Typography variant="h6" sx={{ lineHeight: "24px" }}>
              {data.title}
            </Typography>
            <Typography color={"#adadad"}>
              by{" "}
              <Link
                underline={"hover"}
                target="_blank"
                href={data.supplier_link}
                color={(theme) => theme.palette.custom.link}
              >
                {data.supplier_name}
              </Link>
            </Typography>
            <Box sx={{ display: "flex" }}>
              <Rating
                sx={{ p: "10px 0" }}
                readOnly
                value={data.stars}
                icon={<StarFilledIcon width={22} fill={"#f59c1a"} />}
                emptyIcon={<StarIcon width={22} fill={"#bbbbbb"} />}
              />
              <IconButton
                disableRipple
                onClick={(e) => setAnchorEl(e.currentTarget)}
              >
                <ArrowDropDownIcon />
              </IconButton>
              <Popover
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                onClose={() => setAnchorEl(null)}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "center",
                }}
              >
                <Typography variant="subtitle1" sx={{ p: 2 }}>
                  Star Rating: {data.stars}
                </Typography>
              </Popover>
            </Box>
            <Box sx={{ display: "flex" }}>
              <Typography>
                {data.price.toFixed(2)} {data.currency}{" "}
                <span style={{ color: myTheme.palette.custom.greyText }}>
                  + {data.transport_costs.toFixed(2)} {data.currency} shipping
                </span>
              </Typography>
              <DiscountIcon width={20} style={{ marginLeft: "10px" }} />
            </Box>
            <Typography
              sx={(theme) => ({ color: theme.palette.custom.greyText })}
            >
              all prices incl. {data.vat_percent}% taxes
            </Typography>
          </Box>
          <Box ref={cartRef}>
            <AddToCart onAddToCart={handleAddToCart} />
          </Box>
        </Grid>
      </Grid>
      <SimpleDialog
        open={showFullImage}
        onClose={() => setShowFullImage((prev) => !prev)}
      >
        <PackageIcon width="100%" height={"100%"} fill={previewImage} />
      </SimpleDialog>
    </Container>
  );
};

export default Content;
