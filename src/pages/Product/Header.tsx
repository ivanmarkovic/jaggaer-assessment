import {
  AppBar,
  Badge,
  Container,
  Fade,
  IconButton,
  Typography,
  useTheme,
} from "@mui/material";
import Box from "@mui/material/Box";
import CartIcon from "../../assets/cart.svg?react";
import FavoriteIcon from "../../assets/favorite.svg?react";
import FavoriteFilledIcon from "../../assets/favorite-filled.svg?react";
import FactsSoftIcon from "../../assets/facts-soft.svg?react";
import { ICart } from "../../interfaces/productData";
import AddToCart from "../../components/AddToCart";
import React from "react";
import "../../styles/header.css";

interface IProductHeaderProps {
  id: number;
  title: string;
  favoriteProducts: number[];
  cart: ICart;
  handleAddToCart: (arg0: number) => void;
  showAddToCartButton: boolean;
}

const ProductHeader = ({
  id,
  title,
  favoriteProducts,
  cart,
  handleAddToCart,
  showAddToCartButton,
}: IProductHeaderProps) => {
  const myTheme = useTheme();
  const iconColor = myTheme.palette.custom.icon;
  const [navShadow, setNavShadow] = React.useState(false);
  const [shouldShake, setShouldShake] = React.useState(false);

  React.useEffect(() => {
    window.removeEventListener("scroll", onScroll);
    window.addEventListener("scroll", onScroll, { passive: true });
    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  React.useEffect(() => {
    setShouldShake(true);

    setTimeout(() => {
      setShouldShake(false);
    }, 600);
  }, [cart]);

  const onScroll = () => {
    setNavShadow(window.scrollY > 50);
  };

  return (
    <AppBar
      position="fixed"
      sx={{
        background: "#fff",
        paddingLeft: "12px",
        boxShadow: navShadow ? "0px 4px 10px 0px #939393" : "none",
        borderBottom: "2px solid #efefef",
      }}
    >
      <Container
        maxWidth="lg"
        disableGutters
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography
          variant="h6"
          color={(theme) => theme.palette.orange.main}
          sx={{
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
            overflow: "hidden",
          }}
        >
          {title}
        </Typography>

        <Box sx={{ display: "flex", alignItems: "center" }}>
          {showAddToCartButton && (
            <Fade in={showAddToCartButton}>
              <Box sx={{ mr: "20px", animation: "fadeIn 5s" }}>
                <AddToCart onAddToCart={handleAddToCart} />
              </Box>
            </Fade>
          )}
          <Box sx={{ display: "flex", alignItems: "center", pr: "10px" }}>
            <IconButton>
              {favoriteProducts.includes(id) ? (
                <FavoriteFilledIcon
                  width={24}
                  height={24}
                  fill={myTheme.palette.orange.main}
                />
              ) : (
                <FavoriteIcon width={24} height={24} fill={iconColor} />
              )}
            </IconButton>
            <IconButton>
              <FactsSoftIcon width={30} height={30} fill={iconColor} />
            </IconButton>
          </Box>
          <Box sx={{ borderLeft: "2px solid #ececec", p: "10px" }}>
            <IconButton className={shouldShake ? "shake" : ""}>
              <Badge badgeContent={cart.items} color="orange">
                <CartIcon width={25} height={25} fill={iconColor} />
              </Badge>
            </IconButton>
          </Box>
        </Box>
      </Container>
    </AppBar>
  );
};

export default ProductHeader;
