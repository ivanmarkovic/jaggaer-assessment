import React from "react";
import Header from "./Header";
import { ICart, IProductData } from "../../interfaces/productData";
import Content from "./Content";
import { Box } from "@mui/material";
import BottomContent from "./BottomContent";

interface IProductProps {
  data: IProductData;
}

const Product = (props: IProductProps) => {
  const [cartItems, setCartItems] = React.useState<ICart>({
    items: 0,
    total_costs: 0,
  });
  const [showButtonInNav, setShowButtonInNav] = React.useState(false);

  React.useEffect(() => {
    setCartItems(props.data.cart);
  }, []);

  const handleAddToCart = (pce: number) => {
    setCartItems((prev) => ({
      ...prev,
      items: prev.items + pce,
      total_costs: prev.total_costs + pce * props.data.article.price,
    }));
  };
  return (
    <Box>
      <Header
        id={props.data.article.id}
        title={props.data.article.title}
        favoriteProducts={props.data.user.favorite_articles}
        cart={cartItems}
        handleAddToCart={handleAddToCart}
        showAddToCartButton={showButtonInNav}
      />
      <Content
        data={props.data.article}
        handleAddToCart={handleAddToCart}
        moveAddToCartButton={setShowButtonInNav}
      />
      <BottomContent data={props.data.article} />
    </Box>
  );
};

export default Product;
