import "@material-ui/core/styles";
import { Palette } from "@material-ui/core/styles/createMuiTheme";

declare module "@mui/material/styles" {
  interface Palette {
    custom: {
      icon: string;
      greyText: string;
      greyBg: string;
      link: string;
    };
    orange: PaletteColor;
  }
  interface PaletteOptions {
    custom?: {
      icon?: string;
      greyText?: string;
      greyBg?: string;
      link?: string;
    };
    orange?: PaletteColor;
  }
}

declare module "@mui/material" {
  interface ButtonPropsColorOverrides {
    orange: true;
  }
  interface BadgePropsColorOverrides {
    orange: true;
  }
}
